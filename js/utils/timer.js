'use strict';

const timer = {
    startTime: 0,
    second: 0,
    checkInterval: 100,
    onFrame: null,
    onSecond: null,
    onTime: [],
    interval: 0,
    start: function () {
        if (this.interval) {
            clearInterval(this.interval);
        }
        this.second = 0;
        this.startTime = new Date().getTime();
        this.interval = setInterval(this.frame.bind(this), this.checkInterval);
    },
    stop: function () {
        clearInterval(this.interval);
    },
    resume: function() {
        clearInterval(this.interval);
        this.interval = setInterval(this.frame.bind(this), this.checkInterval);
    },
    frame: function () {
        let now = new Date().getTime();
        if ((now - this.startTime) >= 1000) {
            this.startTime = now;
            this.second ++;
            this.onSecondChange();
        }
        if (this.onFrame) {
            this.onFrame();
        }
    },
    onSecondChange: function () {
        for (let i = 0; i < this.onTime.length; i++) {
            let timeEvent = this.onTime[i];
            if (timeEvent.second == this.second) {
                timeEvent.callThat();
            }
        }
        if (this.onSecond) {
            this.onSecond();
        }
    }

};
