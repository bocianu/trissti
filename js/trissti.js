'use strict';

WebFont.load({
    google: {
        families: ['Oswald::latin-ext', 'Share Tech Mono', 'Molle:400italic:latin']
    },
    active: main
});

const renderer = PIXI.autoDetectRenderer(config.width, config.height, {
    backgroundColor : 0x303040
});
document.body.appendChild(renderer.view);
const stage = new PIXI.Container();
const game = new Game(stage);

function main () {
    game.init();
    gameLoop();
}

function gameLoop () {
    if (game.state == GAME_BOARD) {
        game.board.ballsMove();
        game.board.floatersMove();
        game.board.cellEffects();
    }
    if (game.state == GAME_TITLE) {

    }
    game.gui.fadeLayers();
    requestAnimationFrame(gameLoop);
    renderer.render(stage);
}

