'use strict';

const levels = {
    'journeys': {

        'tutorial': [
            {
                name: 'regular',
                layout: layouts.s10x10_full2,
                ballPool: pools.lvl1,
                ballColors: 5,
                wins: (board)=>When.StarBallsOver(5,board),
                looses: (board)=>When.never(board) //whenTimeOver(120, board)
            },
            {
                name: 'stars',
                layout: layouts.s12x12_full1,
                ballPool: pools.lvl1,
                ballColors: 6,
                wins: (board)=>When.ScoreOver(1000, board),
                looses: (board)=>When.TimeOver(120, board)
            }
        ],

        'capitals_europe': [
            {
                name: 'warsaw',
                layout: layouts.s10x10_full2,
                ballPool: pools.regular,
                ballColors: 5,
                wins: (board)=>When.ScoreOver(1000, board),
                looses: (board)=>When.TimeOver(120, board)
            },
            {
                name: 'prague',
                layout: layouts.s12x12_full1,
                ballPool: pools.lvl1,
                ballColors: 6,
                wins: (board)=>When.ScoreOver(1000, board),
                looses: (board)=>When.TimeOver(120, board)
            }
        ]

    }

};

class When {
    static never (board) {
        if (board === null) return lang.get('never');
        return false;
    };

    static ScoreOver (limit, board) {
        if (board === null) return lang.get('score_over').replace('#', limit);
        return board.score >= limit;
    };

    static TimeOver (limit, board) {
        if (board === null) return lang.get('time_over').replace('#', limit);
        return board.timer.second >= limit;
    };

    static AllCellsClear (board) {
        if (board === null) return lang.get('cells_clear');
        for (var i = 0; i < board.fields.length; i++) {
            if (board.fields[i].c_value > 1) return false;
        }
        return true;
    };

    static StarBallsOver (limit, board) {
        if (board === null) return lang.get('starballs_over').replace('#', limit);
        let ballcount = 0;
        for (let i = 11; i < 20; i++) {
            if (board.counters[i]) {
                ballcount += board.counters[i];
            }
        }
        return ballcount >= limit;
    };
}
