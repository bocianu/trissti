'use strict';

const pool = {
    ballSets: {
        regular: [1, 2, 3, 4, 5, 6, 7],
        black: [10],
        star: [11, 12, 13, 14, 15, 16, 17],
        frozen: [51, 52, 53, 54, 55, 56, 57],
        bomb: [61, 62, 63, 64, 65, 66, 67],
        h_clear: [71, 72, 73, 74, 75, 76, 77],
        v_clear: [81, 82, 83, 84, 85, 86, 87],
        x_clear: [91, 92, 93, 94, 95, 96, 97],
        rainbow: [100],
        b_bomb: [610],
        r_bomb: [6100]
    },

    setPool: function (poolSet, colorLimit) {
        let newpool = [];
        for (let i = 0; i < poolSet.length; i++) {
            let set = poolSet[i];
            if (this.ballSets[set.set] instanceof Array) {
                set.count = set.count || 1;
                for (let count = 0; count < set.count; count++) {
                    let balls = this.ballSets[set.set];
                    if (colorLimit && balls.length > 1) {
                        balls = this.limit(balls, colorLimit);
                    }
                    newpool = newpool.concat(balls);
                }
            }
        }
        return newpool;
    },

    limit: function (balls, colorLimit) {
        let newSet = [];
        for (let i = 0; i < balls.length; i++) {
            let ball = balls[i];
            if ((ball % 10) <= colorLimit) {
                newSet.push(ball);
            }
        }
        return newSet;
    }
};

const pools = {
    regular: [
        {set: 'regular', count: 1}
    ],


    all: [
        {set: 'regular', count: 3},
        {set: 'star'},
        {set: 'frozen'},
        {set: 'bomb'},
        {set: 'h_clear'},
        {set: 'v_clear'},
        {set: 'x_clear'},
        {set: 'black', count: 3},
        {set: 'rainbow', count: 10},
        {set: 'b_bomb', count: 3},
        {set: 'r_bomb', count: 3}
    ],


    lvl1: [
        {set: 'regular', count: 40},
        {set: 'star', count: 2},
        //{ set: 'frozen' },
        //{ set: 'bomb' },
        {set: 'h_clear'},
        {set: 'v_clear'},
        { set: 'x_clear' },
        {set: 'black', count: 20},
        {set: 'rainbow', count: 3}
        //{ set: 'b_bomb', count: 3 },
        //{ set: 'r_bomb', count: 3 },
    ]

};