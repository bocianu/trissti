'use strict';

const lang = {
    current : 'en',
    set : function (ln) {
        this.current = ln;
    },
    get : function(key) {
        if (this[this.current].hasOwnProperty(key)) return this[this.current][key];
        return key;
    }
};

lang.en = {
    'score': 'Score:',
    'time' : 'Time:',
    'moves' : 'Moves:',
    'shuffle' : 'Shuffle',
    'menu' : 'Menu',

    'pause' : 'Pause',
    'resume' : 'Resume Game',
    'restart' : 'Restart Level',
    'quit' : 'Quit Game',

    'journeys': 'Journeys',
    'tutorial': 'Tutorial',
    'regular': 'Basics',
    'stars': 'Star Balls',
    'capitals_europe': 'Capitals in Europe',
    'warsaw': 'Warsaw',
    'prague': 'Prague'

};

lang.pl = {
    'score': 'Punkty:',
    'time' : 'Czas:',
    'moves' : 'Ruchy:',
    'shuffle' : 'Mieszaj',
    'menu' : 'Menu',

    'pause' : 'Pauza',
    'resume' : 'Powrót Do Gry',
    'restart' : 'Jeszcze Raz',
    'quit' : 'Koniec Gry',

    'journeys': 'Podróże',
    'tutorial': 'Samouczek',
    'regular': 'Podstawy',
    'stars': 'Gwiazda',
    'capitals_europe': 'Stolice Europy',
    'warsaw': 'Warszawa',
    'prague': 'Praga'
};
