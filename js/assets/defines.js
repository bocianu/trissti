'use strict';

const BALL_BLACK = 10;
const BALL_RAINBOW = 100;
const BOMB_BLACK = 610;
const BOMB_RAINBOW = 6100;

const GAME_TITLE = 0;
const GAME_BOARD = 1;
const GAME_PAUSED = 99;

const colors = {
    0: 'empty',
    1: 'red',
    2: 'green',
    3: 'blue',
    4: 'yellow',
    5: 'purple',
    6: 'brown',
    7: 'ivory',
    10: 'black'
};

