'use strict';

const config = {
    lang: 'pl',
    width: 1000,
    height: 700,
    cell_border: 2,
    offset_x: 180,
    offset_y: 30,
    move_speed: 7
};

