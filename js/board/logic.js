'use strict';

class LogicAI {

    areDifferent (cell1, cell2) {
        return cell1.c_contains != cell2.c_contains;
    };

    areNeighbours (cell1, cell2) {
        if (cell1.c_value == 0 || cell2.c_value == 0) {
            return false;
        }
        if (cell1.c_x == cell2.c_x && Math.abs(cell1.c_y - cell2.c_y) == 1) {
            return true;
        }
        return !!(cell1.c_y == cell2.c_y && Math.abs(cell1.c_x - cell2.c_x) == 1);

    };

    canSwap (cell1, cell2) {
        if (cell1.c_value == 0 || cell2.c_value == 0) {
            return false;
        }
        this.ballSwitch(cell1, cell2);
        let can = this.isBallInLine(cell1) || this.isBallInLine(cell2);
        this.ballSwitch(cell1, cell2);
        return can;
    };

    sameColour (cell1, cell2) {
        if (cell1.c_value == 0 || cell2.c_value == 0) {
            return false;
        }
        if (cell1.c_contains == BALL_BLACK || cell2.c_contains == BALL_BLACK) {
            return false;
        }
        if (cell1.c_contains == BALL_RAINBOW || cell2.c_contains == BALL_RAINBOW) {
            return true;
        }
        return cell1.c_contains % 10 == cell2.c_contains % 10;
    };

    same3Colour (cell1, cell2, cell3) {
        return (this.sameColour(cell1, cell2) && this.sameColour(cell2, cell3) && this.sameColour(cell1, cell3));
    };

// returns true if cell is a part of line (ready to remove)
    isBallInLine (cell) {
        // x - axis test

        // 0 1 1 1 0
        let cell_1x_l = this.getXY(cell.c_x - 1, cell.c_y);
        let cell_1x_r = this.getXY(cell.c_x + 1, cell.c_y);
        if ((cell_1x_l && cell_1x_r) && this.same3Colour(cell, cell_1x_l, cell_1x_r)) {
            return true;
        }
        // 1 1 1 0 0
        let cell_2x_l = this.getXY(cell.c_x - 2, cell.c_y);
        if ((cell_1x_l && cell_2x_l) && this.same3Colour(cell, cell_1x_l, cell_2x_l)) {
            return true;
        }
        // 0 0 1 1 1
        let cell_2x_r = this.getXY(cell.c_x + 2, cell.c_y);
        if ((cell_1x_r && cell_2x_r) && this.same3Colour(cell, cell_1x_r, cell_2x_r)) {
            return true;
        }

        // y - axis test

        // 0 1 1 1 0
        let cell_1y_l = this.getXY(cell.c_x, cell.c_y - 1);
        let cell_1y_r = this.getXY(cell.c_x, cell.c_y + 1);
        if ((cell_1y_l && cell_1y_r) && this.same3Colour(cell, cell_1y_l, cell_1y_r)) {
            return true;
        }
        // 1 1 1 0 0
        let cell_2y_l = this.getXY(cell.c_x, cell.c_y - 2);
        if ((cell_1y_l && cell_2y_l) && this.same3Colour(cell, cell_1y_l, cell_2y_l)) {
            return true;
        }
        // 0 0 1 1 1
        let cell_2y_r = this.getXY(cell.c_x, cell.c_y + 2);
        if ((cell_1y_r && cell_2y_r) && this.same3Colour(cell, cell_1y_r, cell_2y_r)) {
            return true;
        }

        return false;
    };

// finds and returns lines to remove
    linesGet () {
        let lines = [];
        let x, y, last, last2, size, cell, line;
        // horizontal
        for (y = 0; y < this.height; y++) {
            last = 0;
            last2 = 0;
            size = 0;
            for (x = 0; x < this.width; x++) {
                cell = this.getXY(x, y);
                if (this.sameColour(cell, {c_contains: last}) && last != 0) {
                    size++;
                    if (size == 2 && this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        line = {
                            cells: [],
                            dir: 'horizontal',
                            color: colors[last]
                        };
                        line.cells.push(this.getXY(x - 2, y));
                        line.cells.push(this.getXY(x - 1, y));
                        line.cells.push(cell);
                        lines.push(line);
                    }
                    if (size > 2 && this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        line.cells.push(cell);
                    }
                    if (size > 1 && !this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        size = 0;
                        x--;
                        continue;
                    }
                } else {
                    size = 0;
                }
                last2 = last;
                last = cell.c_contains;
            }
        }

        // vertical
        for (x = 0; x < this.width; x++) {
            last = 0;
            size = 0;
            for (y = 0; y < this.height; y++) {
                cell = this.getXY(x, y);
                if (this.sameColour(cell, {c_contains: last}) && last != 0) {
                    size++;
                    if (size == 2 && this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        line = {
                            cells: [],
                            dir: 'vertical',
                            color: colors[last]
                        };
                        line.cells.push(this.getXY(x, y - 2));
                        line.cells.push(this.getXY(x, y - 1));
                        line.cells.push(cell);
                        lines.push(line);
                    }
                    if (size > 2 && this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        line.cells.push(cell);
                    }
                    if (size > 1 && !this.same3Colour(cell, {c_contains: last}, {c_contains: last2})) {
                        size = 0;
                        y--;
                        continue;
                    }
                } else {
                    size = 0;
                }
                last2 = last;
                last = cell.c_contains;
            }
        }
        return lines;
    };

// fills empty playfield
    fillEmpty (callback) {
        let drops = this.dropsGet();
        if (drops.length > 0) {
            for (let x = 0; x < drops.length; x++) {
                this.dropColumn(drops[x]);
            }
            if (callback) {
                this.falling = true;
                this.fallcallback = callback;
            }
        }
    };

// finds and returns columns to drop new balls
    dropsGet () {
        let drops = [];
        for (let x = 0; x < this.width; x++) {
            for (let y = this.height - 1; y >= 0; y--) {
                let cell = this.getXY(x, y);
                if (cell.c_value != 0 && cell.c_contains == 0) {
                    drops.push(x);
                    break;
                }
            }
        }

        return drops;
    };

// returns array of possible moves (cell pairs)
    getPossibleMoves () {
        var moves = [];
        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                let cell = this.getXY(x, y);
                if (cell.c_value != 0) {
                    if (x < this.width - 1) {
                        let cellx = this.getXY(x + 1, y);
                        if (cellx.c_value != 0)
                            if (this.canSwap(cell, cellx)) {
                                moves.push({cell1: cell, cell2: cellx});
                            }
                    }
                    if (y < this.height - 1) {
                        let celly = this.getXY(x, y + 1);
                        if (celly.c_value != 0)
                            if (celly.c_value != 0 && this.canSwap(cell, celly)) {
                                moves.push({cell1: cell, cell2: celly});
                            }
                    }
                }
            }

        }
        return moves;
    };
}
