'use strict';

class Board extends BoardGui {

    constructor (stage) {
        super();
        this.fields = [];
        this.container = null;
        this.timer = timer;
        this.timer.onSecond = () => {
            this.updateTimer();
            this.checkWinLoose();
        };
        this.stage = stage;
        this.counters = {};
    }

    levelLoad (level) {
        this.level = level;
        this.result = false;
        this.paused = false;
        this.width = level.layout.width;
        this.height = level.layout.height;
        this.cellsize = (config.height - (config.cell_border*this.height) - (config.offset_y *2) ) / this.height;
        this.fields = [];
        this.counters = {};
        this.layout = level.layout.tiles;
        this.ballPool = pool.setPool(level.ballPool, level.ballColors);
        this.moves = [];

        this.floaters = [];
        this.labelDisplay = {};
        this.buttons = [];
        this.falling = false;
        this.selectedCell = null;
        this.score = 0;
        this.shuffles = level.layout.shuffles || 0;

        if (this.container) {
            this.stage.removeChildren();
        }
        this.container = new PIXI.Container();
        this.container.position.x = config.offset_x + (this.cellsize / 2);
        this.container.position.y = config.offset_y + (this.cellsize / 2);
        this.stage.addChild(this.container);
        for (var y = 0; y < this.height; y++) {
            for (var x = 0; x < this.width; x++) {
                let field = this.getXY(x, y);
                if (field && field.destroy) {
                    field.destroy()
                }
                let cell;
                let val = this.layout[x + (y * this.width)];
                cell = PIXI.Sprite.fromImage('assets/cell_' + val + '.png');
                cell.x = (this.cellsize + config.cell_border) * x;
                cell.y = (this.cellsize + config.cell_border) * y;
                cell.tint = 0xffffff;
                cell.anchor.x = 0.5;
                cell.anchor.y = 0.5;
                cell.width = this.cellsize;
                cell.height = this.cellsize;
                cell.c_x = x;
                cell.c_y = y;
                cell.c_ball = null;
                cell.c_value = val;
                cell.c_contains = 0;
                cell.c_cell = this.layout[x + (y * this.width)];
                cell.c_selected = false;
                cell.interactive = true;
                cell.c_contains = 0;
                cell.on('mousedown', () => {
                    this.cellClicked(cell)
                }, this);
                cell.zIndex = 10;
                this.container.addChild(cell);
                this.setXY(x, y, cell);
                Gui.updateLayersOrder(this.container);
            }
        }
        let _self = this;
        this.timer.start();
        this.labelsShow();
        this.fill();

    }

    setXY (x, y, data) {
        this.fields[(y * this.width) + x] = data;
    };

    getXY (x, y) {
        if (x < 0 || x >= this.width || y < 0 || y >= this.height) {
            return false;
        }
        return this.fields[(y * this.width) + x];
    };

    ballsRandomize () {
        for (var y = 0; y < this.height; y++) {
            for (var x = 0; x < this.width; x++) {
                let cell = this.getXY(x, y);
                if (cell.c_value != 0) {
                    this.ballSet(x, y, this.ballPickRandom());
                }
            }
        }
    };

    fill () {
        this.ballsRandomize();
        let lines = this.linesGet();
        while (lines.length > 0) {
            this.linesRemove(lines, false);
            this.fillEmpty(false);
            lines = this.linesGet();
        }
        this.moves = this.getPossibleMoves();
        this.updateMoves();
    };

    clearLines (scoring) {
        let lines = this.linesGet();
        if (lines.length > 0) {
            this.linesRemove(lines, scoring);
            this.fillEmpty(() => {
                this.clearLines(scoring)
            });
        } else {
            this.moves = this.getPossibleMoves();
            this.updateMoves();
        }
        this.checkWinLoose();
    };

    linesRemove (rlines, scoring) {
        // count scores ... fire bonuses
        const toRemove = [];
        for (var i = 0; i < rlines.length; i++) {
            let line = rlines[i];
            if (scoring) {
                let score = this.scoring(line);
                this.addScore(score);
                this.addScoreFloater(line, score);
            }
            for (var j = 0; j < line.cells.length; j++) {
                let cell = line.cells[j];
                toRemove.push(cell);
                let tenthDigit = Math.floor(cell.c_contains / 10);
                // horizontal clear
                if (tenthDigit == 7 || tenthDigit == 9) {
                    this.lineRemoveHorizontal(cell.c_y, toRemove);
                }
                // vertical clear
                if (tenthDigit == 8 || tenthDigit == 9) {
                    this.lineRemoveVertical(cell.c_x, toRemove);
                }
            }
        }
        // remove balls
        for (i = 0; i < toRemove.length; i++) {
            let cell = toRemove[i];
            if (cell.c_contains != 0) {
                this.ballRemove(cell.c_x, cell.c_y, scoring);
            }
        }
        Gui.updateLayersOrder(this.container);
    };

    lineRemoveHorizontal (y, toRemove) {
        for (let x = 0; x < this.width; x++) {
            toRemove.push(this.getXY(x, y));
        }
    };

    lineRemoveVertical (x, toRemove) {
        for (let y = 0; y < this.height; y++) {
            toRemove.push(this.getXY(x, y));
        }
    };

    shuffle () {
        if (!this.paused) {
            let pool = [];
            for (let x = 0; x < this.width; x++) {
                for (let y = 0; y < this.height; y++) {
                    let cell = this.getXY(x, y);
                    if (cell.c_value != 0) {
                        pool.push(cell.c_ball);
                    }
                }
            }
            for (let x = 0; x < this.width; x++) {
                for (let y = 0; y < this.height; y++) {
                    let cell = this.getXY(x, y);
                    if (cell.c_value != 0) {
                        var rnd = Math.floor(Math.random() * pool.length);
                        var rball = pool.splice(rnd, 1)[0];
                        // console.log(rball.x);
                        rball.c_xmove = this.getXY(x, y).x - rball.x;
                        rball.c_ymove = this.getXY(x, y).y - rball.y;
                        this.fields[(y * this.width) + x].c_ball = rball;
                        this.fields[(y * this.width) + x].c_contains = rball.c_value;
                    }
                }
            }
            this.fallcallback = () => {
                // console.log('callback');
                this.clearLines(false);
            };
            this.falling = true;
        }
    };

// ****************** BALLS
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

    ballPickRandom () {
        return this.ballPool[Math.floor(Math.random() * this.ballPool.length)];
    };

    ballSet (x, y, val) {
        let cell = this.getXY(x, y);
        cell.c_contains = val;
        let ball = PIXI.Sprite.fromImage('assets/ball_' + cell.c_contains + '.png');
        ball.x = ((this.cellsize + config.cell_border) * x);
        ball.y = ((this.cellsize + config.cell_border) * y);
        ball.anchor.x = 0.5;
        ball.anchor.y = 0.5;
        ball.c_xmove = 0;
        ball.c_ymove = 0;
        ball.velocity = 0;
        ball.c_value = val;
        ball.width = this.cellsize;
        ball.height = this.cellsize;
        ball.zIndex = Math.floor(100 + y);
        cell.c_ball = ball;
        this.container.addChild(ball);
        Gui.updateLayersOrder(this.container);
        return ball
    };

    ballRemove (x, y, scoring) {
        let cell = this.getXY(x, y);
        let ball = cell.c_ball;
        if (cell.c_contains != 0) {
            cell.c_ball = null;
            cell.c_contains = 0;
            cell.tint = 0xff0000;
            ball.zIndex = 900;
            this.floaters.push({
                counter: 280,
                fadeout: -0.02,
                scale: -0.01,
                obj: ball
            });
            if (scoring) {
                this.counters[ball.c_value] = (this.counters[ball.c_value])?this.counters[ball.c_value]+1:1;
                if (cell.c_value > 1) {
                    this.dropCellValue(cell);
                }
            }
        }
    };

    dropCellValue (cell) {
        cell.c_value--;
        cell.texture = PIXI.Texture.fromImage('assets/cell_' + cell.c_value + '.png');
    };

    ballSwitch (cell1, cell2) {
        let ball1 = cell1.c_ball;
        let ball2 = cell2.c_ball;
        // console.log(cell1, cell2);
        cell1.c_contains = ball2.c_value;
        cell2.c_contains = ball1.c_value;
        cell1.c_ball = ball2;
        cell2.c_ball = ball1;
    };

    dropColumn (x) {
        let newballs = 0;
        let freeslots = [];
        for (let y = this.height - 1; y >= 0; y--) {
            let cell = this.getXY(x, y);
            if (cell.c_value != 0) {
                if (cell.c_contains == 0) {
                    freeslots.push(y);
                    newballs++;
                }
                if (cell.c_contains != 0 && freeslots.length > 0) {
                    let fall = freeslots.shift() - y;
                    //console.log(fall);
                    freeslots.push(y);
                    this.dropBall(x, y, fall);
                }
            }
        }
        let startPos = 1;
        while (newballs > 0) {
            newballs--;
            let fall = freeslots.shift();
            //console.log('fall :'+fall);
            let ball = this.ballSet(x, fall, this.ballPickRandom());
            ball.y = (-startPos * (this.cellsize + config.cell_border));
            ball.c_ymove = ((this.cellsize + config.cell_border) * (fall + startPos));
            startPos++;
        }
    };

    dropBall (x, y, fall) {
        let dst_cell = this.getXY(x, y + fall);
        let src_cell = this.getXY(x, y);
        let ball = src_cell.c_ball;
        ball.c_ymove += (this.cellsize + config.cell_border) * fall;
        dst_cell.c_contains = src_cell.c_contains;
        src_cell.c_contains = 0;
        dst_cell.c_ball = ball;
        src_cell.c_ball = null;
    };

// ****************** SCORING
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


    scoring (line) {
        let score = 0;
        let multiplier = 1;
        score += line.cells.length * 10;
        for (let i = 0; i < line.cells.length; i++) {
            // star ball
            if (Math.floor(line.cells[i].c_contains / 10) == 1) {
                multiplier += 1;
            }
            // rainbow ball
            if (line.cells[i].c_contains == 100) {
                score += 100;
            }
        }
        return score * multiplier;
    };

    checkWinLoose () {
        if (this.level) {
            if (this.level.wins(this)) {
                this.wins();
            }
            if (this.level.looses(this)) {
                this.looses();
            }
        }
    };

    wins () {
        if (!this.result) {
            console.log('winner');
            this.result = true;
        }
    };

    looses () {
        if (!this.result) {
            console.log('looser');
            this.result = true;
        }
    };
}
