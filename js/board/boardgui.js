'use strict';

class BoardGui extends LogicAI {
    constructor () {
        super();
        this.floaters = [];
        this.labelDisplay = {};
        this.buttons = [];
        this.falling = false;

    };

    labelsShow () {
        var trisstiLabel = new PIXI.Text(' trissti ', {
            font: '42px Molle',
            fill: 0xddddff,
            stroke: 0x4444aa,
            strokeThickness: 8,
            align: 'center'
        });
        trisstiLabel.anchor.x = 0.5;
        trisstiLabel.y = config.offset_y - 12;
        trisstiLabel.x = (config.offset_x - 10) / 2;
        this.stage.addChild(trisstiLabel);

        this.printLabel('score', 70);
        this.printDisplay('0', 100, 'score');

        this.printLabel('time', 160);
        this.printDisplay('0:00', 190, 'timer');

        this.printLabel('moves', 250);
        this.printDisplay('0', 280, 'moves');

        this.printButton('shuffle', 30, 550, 120, 40, 'shuffle')
            .on('mousedown', this.shuffle, this);

        this.printButton('menu', 30, 600, 120, 40, 'menu')
            .on('mousedown', ()=> {
                this.select({action:'showInGameMenu'});
            });

    };

    menuShow() {
        let popup = Gui.getRectangle( config.width /2 - 200, 100, 400, 400);
        popup.filters = [Gui.shadowDeep];

        let label = Gui.getText('pause',200,20,50);
        popup.addChild(label);

        let resume = Gui.getButton('resume',100,120,200,40).on('mouseup', ()=> {
            this.select({action:'inGameResume'});
        });
        popup.addChild(resume);
        let restart = Gui.getButton('restart',100,170,200,40).on('mouseup', ()=> {
            this.select({action:'inGameRestart'});
        });
        popup.addChild(restart);
        let quit = Gui.getButton('quit',100,220,200,40).on('mouseup', ()=> {
            this.select({action:'inGameQuit'});
        });
        popup.addChild(quit);

        this.stage.addChild(popup);
        this.container.visible = false;

        this.popup = popup;
    };

    menuHide() {
        this.stage.removeChild(this.popup);
        this.container.visible = true;
    };

    printButton (txt, x, y, w, h, name) {

        let buttonContainer = Gui.getButton(txt, x, y, w, h);

        this.stage.addChild(buttonContainer);
        this.buttons[name] = buttonContainer;
        this.labelDisplay[name] = txt;
        return buttonContainer;
    };

    select () {
        console.log('not overloaded');
    };

    printLabel (txt, y) {
        let label = Gui.getText(txt, (config.offset_x - 10) / 2, config.offset_y + y, 18);
        this.stage.addChild(label);
    };

    printDisplay (txt, y, name) {
        var label = new PIXI.Text(txt, {
            font: 'bold 30px "Share Tech Mono"',
            fill: 0xddddff,
            stroke: 0x8888ff,
            strokeThickness: 2,
            align: 'center'
        });
        label.anchor.x = 0.5;
        label.y = config.offset_y + y;
        label.x = (config.offset_x - 10) / 2;
        this.stage.addChild(label);
        this.labelDisplay[name] = label;
        return label;
    };

    addScoreFloater (line, scoreValue) {
        let score = new PIXI.Text(scoreValue, {
            font: 'bold ' + Math.floor(config.cell_size / 2) + 'px Oswald',
            fill: 0xffccaa,
            stroke: 0x000000,
            strokeThickness: 1,
            align: 'center'
        });
        score.y = line.cells[0].y;
        score.x = line.cells[0].x;
        score.anchor.x = 0.5;
        score.anchor.y = 0.5;
        if (line.dir == 'horizontal') {
            score.x += Math.floor((line.cells.length - 1) * ((config.cell_size + config.cell_border) / 2));
        }
        if (line.dir == 'vertical') {
            score.y += Math.floor((line.cells.length - 1) * ((config.cell_size + config.cell_border) / 2));
        }
        score.zIndex = 1000;
        this.container.addChild(score);
        Gui.updateLayersOrder(this.container);
        this.floaters.push({
            counter: 60,
            fadeout: -0.001,
            scale: +0.015,
            obj: score
        });
    };

    setScore (val) {
        this.score = val;
        this.labelDisplay.score.text = val;
    };

    addScore (val) {
        this.setScore(this.score + val);
    };

    updateTimer () {
        this.labelDisplay.timer.text = Math.floor(this.timer.second / 60) +
            ":" +
            ('0' + this.timer.second % 60).slice(-2);
    };

    updateMoves () {
        this.labelDisplay.moves.text = this.moves.length;
    };

    cellClicked (cell) {
        if (cell.c_contains != 0 && !this.falling && !this.paused) {
            this.ballSelect(cell);
        }
    };

    ballSelect (cell) {
        if (cell.selectedCell) {
            cell.selectedCell = false;
            this.selectedCell = null;
            this.ballSize(cell.c_ball, -1);
        } else if (this.selectedCell === null) {
            cell.selectedCell = true;
            this.selectedCell = cell;
            this.ballSize(cell.c_ball, 1);
        } else {
            if (this.ballSwap(cell)) {
                this.clearLines(true);
            }
        }
    };

    ballSize (ball, sign) {
        ball.scale.x += 0.2 * sign;
        ball.scale.y += 0.2 * sign;
    };

    ballSwap (cell) {
        let cell1 = this.selectedCell;
        let cell2 = cell;
        if (this.areDifferent(cell1, cell2) && this.areNeighbours(cell1, cell2) && this.canSwap(cell1, cell2)) {
            this.ballSize(cell1.c_ball, -1);
            let ball1 = cell1.c_ball;
            let ball2 = cell2.c_ball;
            ball1.c_xmove = ball2.x - ball1.x;
            ball1.c_ymove = ball2.y - ball1.y;
            ball2.c_xmove = ball1.x - ball2.x;
            ball2.c_ymove = ball1.y - ball2.y;
            cell1.selectedCell = false;
            cell2.selectedCell = false;
            this.selectedCell = null;
            this.ballSwitch(cell1, cell2);
            return true;
        } else {
            this.wrongMove(cell);
        }
        return false;
    };

    wrongMove () {
        let cell1 = this.selectedCell;
        this.ballSize(cell1.c_ball, -1);
        cell1.selectedCell = false;
        this.selectedCell = null;
    };

// ****************** ANIMATIONS - LOOP routines
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

    ballMove (ball) {
        let moved = false;
        // X move
        if (ball.c_xmove < 0) {
            moved = true;
            ball.x -= config.move_speed;
            ball.c_xmove += config.move_speed;
            if (ball.c_xmove > 0) {
                ball.x += ball.c_xmove;
                ball.c_xmove = 0;
            }
        }

        if (ball.c_xmove > 0) {
            moved = true;
            ball.x += config.move_speed;
            ball.c_xmove -= config.move_speed;
            if (ball.c_xmove < 0) {
                ball.x += ball.c_xmove;
                ball.c_xmove = 0;
            }
        }

        // Y move
        if (ball.c_ymove < 0) {
            moved = true;
            ball.y -= config.move_speed;
            ball.c_ymove += config.move_speed;
            if (ball.c_ymove > 0) {
                ball.y += ball.c_ymove;
                ball.c_ymove = 0;
            }
        }

        if (ball.c_ymove > 0) { // fall
            moved = true;
            ball.velocity += 0.3;
            ball.y += ball.velocity;
            ball.c_ymove -= ball.velocity;
            if (ball.c_ymove < 0) {
                ball.y += ball.c_ymove;
                ball.c_ymove = 0;
                ball.velocity = 0;
            }
        }

        return moved;
    };

    ballsMove () {
        let moved = false;
        for (var i = 0; i < this.fields.length; i++) {
            var cell = this.fields[i];
            if (cell.c_contains != 0) {
                let ball = cell.c_ball;
                if (this.ballMove(ball)) {
                    moved = true;
                }
            }
        }
        if (this.falling && !moved) {
            this.falling = false;
            let cb = this.fallcallback;
            if (cb) {
                this.fallcallback = false;
                cb();
            }
        }
    };

    floatersMove () {
        for (var i = 0; i < this.floaters.length; i++) {
            let floater = this.floaters[i];
            if (floater) {
                floater.counter -= 1;
                floater.obj.alpha += floater.fadeout;
                floater.obj.scale.x += floater.scale;
                floater.obj.scale.y += floater.scale;
                this.ballMove(floater.obj);
                if (floater.counter == 0) {
                    this.container.removeChild(floater.obj);
                    this.floaters.splice(i, 1);
                }
            }
        }
    };

    cellEffects () {
        for (var i = 0; i < this.fields.length; i++) {
            var cell = this.fields[i];
            if (cell.tint < 0xffffff) {
                cell.tint += 0x001111 / 2;
            }
        }

    };
}