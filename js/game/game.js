'use strict';

class Game {
    constructor (stage) {
        this.player = new Player();
        this.gui = new GameGui(stage);
        this.gui.select = this.select.bind(this);
        this.board = new Board(this.gui.layers['board']);
        this.board.select = this.select.bind(this);
        this.state = null;
        lang.set(config.lang);
    };

    init() {
        this.state = GAME_TITLE;
        this.gui.titleShow();
    };

    select(params) {
        if (params && params.action) {
            this[params.action]();
        }
    };

    startLevel() {
        let lvl =  levels.journeys.tutorial[Math.floor(Math.random() * levels.journeys.tutorial.length)];
        this.initBoard(lvl);
        this.gui.titleHide();
        this.gui.boardShow();
        this.state = GAME_BOARD;
    };

    initBoard (level) {
        this.board.levelLoad(level);
    };

    showInGameMenu() {
        if (this.state != GAME_PAUSED) {
            this.state = GAME_PAUSED;
            // console.log('menu');
            this.board.timer.stop();
            this.board.paused = true;
            this.board.menuShow();
        }
    };

    inGameResume() {
        this.board.menuHide();
        this.state = GAME_BOARD;
        this.board.timer.resume();
        this.board.paused = false;
    }

    inGameRestart() {
        this.board.menuHide();
        this.board.levelLoad(this.board.level);
        this.state = GAME_BOARD;
    }

    inGameQuit() {
        this.board.menuHide();
        this.gui.boardHide();
        this.state = GAME_TITLE;
        this.gui.titleShow();
    }


}

