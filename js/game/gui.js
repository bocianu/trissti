'use strict';

class Gui {
    static getButton (txt, x, y, w, h) {
        txt = lang.get(txt);
        let buttonContainer = Gui.getRectangle(x, y, w, h, 0x444466, 0x555577, 2);
        let label = Gui.getText(txt, w / 2, h / 4, 18);
        label.filters = [Gui.shadow];
        buttonContainer.addChild(label);
        buttonContainer.interactive = true;
        buttonContainer.filters = [Gui.shadow];
        return buttonContainer;
    }

    static getText (txt, x, y, size) {
        txt = lang.get(txt);
        let label = new PIXI.Text(txt, {
            font: size + 'px Oswald',
            fill: 0xaaaaff,
            align: 'center'
        });
        label.anchor.x = 0.5;
        label.x = x;
        label.y = y;
        return label;
    };

    static getRectangle (x, y, w, h, fill = 0x666688, outline = 0x555577, outlineWidth = 12) {
        let rectangleContainer = new PIXI.Container();
        rectangleContainer.x = x;
        rectangleContainer.y = y;
        rectangleContainer.width = w;
        rectangleContainer.height = h;
        let graphics = new PIXI.Graphics();
        graphics.lineStyle(outlineWidth, outline, 1);
        graphics.beginFill(fill, 1);
        graphics.drawRoundedRect(0, 0, w, h, 5);
        graphics.endFill();
        rectangleContainer.addChild(graphics);
        return rectangleContainer;
    };

    static updateLayersOrder (container) {
        container.children.sort(function (a, b) {
            a.zIndex = a.zIndex || 0;
            b.zIndex = b.zIndex || 0;
            return a.zIndex - b.zIndex
        });
    };
}

Gui.shadow = new PIXI.filters.DropShadowFilter();
Gui.shadow.alpha = 0.7;
Gui.shadow.angle = 45 * Math.PI / 180;
Gui.shadow.blur = 7;
Gui.shadow.distance = 3;
Gui.shadow.color = 0x000000;

Gui.shadowDeep = new PIXI.filters.DropShadowFilter();
Gui.shadowDeep.alpha = 0.9;
Gui.shadowDeep.angle = 45 * Math.PI / 180;
Gui.shadowDeep.blur = 20;
Gui.shadowDeep.distance = 5;
Gui.shadowDeep.color = 0x000000;


class GameGui {
    constructor (stage) {
        this.stage = stage;
        this.layers = {'board': null, 'title': null, 'game1': null};
        this.initLayers();
        this.titleDraw(this.layers['title']);
    };

    initLayers () {
        for (let layer in this.layers) {
            this.layers[layer] = new PIXI.Container();
            this.layers[layer].visible = false;
            this.layers[layer].fadeTo = 0;
            this.stage.addChild(this.layers[layer]);
        }
    };

    // *********************************************************** TITLE

    titleDraw (stage) {
        let titlebox = Gui.getRectangle(0, 0, config.width, config.height);

        var trisstiLabel = new PIXI.Text(' trissti ', {
            font: '180px Molle',
            fill: 0xddddff,
            stroke: 0x444466,
            strokeThickness: 18,
            align: 'center'
        });
        trisstiLabel.anchor.x = 0.5;
        trisstiLabel.y = -20;
        trisstiLabel.x = config.width / 2;
        trisstiLabel.filters = [Gui.shadowDeep];
        titlebox.addChild(trisstiLabel);

        let start = Gui.getButton('Start', config.width / 2 - 60, 200, 120, 40);
        start.on('mousedown', this.startGame, this);

        titlebox.addChild(start);
        stage.addChild(titlebox);
    }

    startGame () {
        this.select({action: 'startLevel'});
    }

    select () {
        console.log('not overloaded');
    }

    titleShow () {
        this.layers['title'].alpha = 0;
        this.layers['title'].fadeTo = 1;
        this.layers['title'].visible = true;
    };

    titleHide () {
        this.layers['title'].fadeTo = 0;
        //this.layers['title'].visible = false;
    }

    boardShow () {
        this.layers['board'].alpha = 0;
        this.layers['board'].fadeTo = 1;
        this.layers['board'].visible = true;
    }

    boardHide () {
        this.layers['board'].fadeTo = 0;
        //this.layers['board'].visible = false;
    }


// ****************** ANIMATIONS - LOOP routines
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

    fadeLayers () {
        for (let layer in this.layers) {
            if (this.layers[layer].fadeTo != this.layers[layer].alpha) {
                let delta = 0.03;
                if (this.layers[layer].fadeTo == 0) delta = -delta;
                this.layers[layer].alpha += delta;
                if (this.layers[layer].alpha < 0.01) {
                    this.layers[layer].alpha = 0;
                    this.layers[layer].visible = false;
                }
                if (Math.abs(this.layers[layer].alpha) > 0.99) {
                    this.layers[layer].alpha = 1;
                }
            }
        }
    }

}